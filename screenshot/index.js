'use strict';

/* eslint no-restricted-syntax: ["off"] */
/* eslint no-await-in-loop: ["off"] */

const path = require('path');
const webdriverio = require('webdriverio');

const options = {
	desiredCapabilities: {
		/*browserName: 'chrome',
		chromeOptions: {
			args: [
				//'--headless',
				'--disable-gpu',
				'--window-size=1920,1080',
			],
			binary: 'C:\\Program Files (x86)\\Google\\Chrome\\Application\\chrome.exe',
		},*/

		// https://github.com/mozilla/geckodriver#firefox-capabilities
		// https://developer.mozilla.org/en-US/docs/Mozilla/Command_Line_Options#-headless
		browserName: 'firefox',
		'moz:firefoxOptions': {
			args: [
				//'-headless',
				//'-screenshot',
				//'--window-size=1920,1080',
			],
			binary: 'C:\\Program Files\\Mozilla Firefox\\firefox.exe',
			log: { level: 'trace' },
		},
	},
};
const client = webdriverio.remote(options);

// https://developers.google.com/maps/documentation/urls/guide#map-action
// https://stackoverflow.com/questions/2660201/what-parameters-should-i-use-in-a-google-maps-url-to-go-to-a-lat-lon
/*const arr = [
	'https://www.google.com/maps/@?api=1&map_action=map&center=46.9085436,19.6952169&zoom=20&basemap=satellite&layer=none',

	//'https://www.google.com/maps/place/46%C2%B054'30.8%22N+19%C2%B041'42.8%22E/@46.9085647,19.6948024,132m/data=!3m1!1e3!4m5!3m4!1s0x0:0x0!8m2!3d46.9085436!4d19.6952169',
	//'http://maps.google.com/maps?z=19&t=e&q=loc:46.9085436+19.6952169',

	//'https://www.google.com/maps/search/?api=1&query=46.9085436,19.6952169&z=20',

	//'https://www.google.hu/maps/@46.9085436,19.6952169,100m/data=!3m1!1e3?hl=hu&om=0&t=m',
	//'https://www.google.hu/maps/@46.9084999,19.6950863,100m/data=!3m1!1e3?hl=hu&om=0&t=m',
	//'https://earth.google.com/web/@46.90852471,19.69517286,132.2466637a,130.17143577d,35y,0h,0t,0r',
	//'https://duckduckgo.com/', 'https://jakabszallas.hu/', 'https://mesziair.hu/',
];*/

const coords = [
	['46.9085436', '19.6952169'],
];

const urls = [];

for (let i = 0; i < coords.length; i += 1) {
	urls.push(`https://www.google.com/maps/@?api=1&map_action=map&center=${coords[i][0]},${coords[i][1]}&zoom=20&basemap=satellite&layer=none`);
}

console.log(urls);

/*client.init().then(() => {
	return arr.map((value, i) => {
		//console.log(`value: ${value}\ni: ${i}`);
		return client
			.url(value)
			.execute(function () {
				const styleElement = document.createElement('style');
				document.head.appendChild(styleElement);
				styleElement.sheet.insertRule('::-webkit-scrollbar { display: none; }');
			})
			.saveScreenshot(path.join(__dirname, `googleScreenshot-loop-${i}.png`));
	});
}).then((arrPromise) => {
	//console.log(arrPromise);
	//console.log(client);
	return Promise.all(arrPromise);
}).then(() => {
	return client.end();
}).catch((reason) => {
	console.log(reason);
});*/

function sleep(ms) {
	return new Promise(resolve => setTimeout(resolve, ms));
}

const init = async (array) => {
	try {
		await client.init();

		let i = 0;
		for (const value of array) {
			//console.log(`value: ${value}\ni: ${i}`);
			await client.url(value);
			// http://webdriver.io/api/protocol/windowHandleSize.html
			// http://webdriver.io/api/window/setViewportSize.html
			await client.windowHandleSize({ width: 1920, height: 1080 }); // gecko
			await client.click('#omnibox button.searchbox-hamburger');
			await client.execute(() => {
				/*const styleElement = document.createElement('style');
				document.head.appendChild(styleElement);*/
				/*styleElement.sheet.insertRule(`
					::-webkit-scrollbar { display: none; }
				`);*/
				/*styleElement.sheet.insertRule(`
					::-webkit-scrollbar { display: none; }
				`, 0);*/

				const css = document.createElement('style');
				css.type = 'text/css';
				css.innerHTML = `
					body { overflow: hidden; }

					/* Google Maps in Firefox */
					#omnibox-container,
					#vasquette,
					#fineprint,
					#scale,
					#minimap,
					#watermark,
					.app-viewcard-strip {
						display: none !important;
					}

					/* Google Earth in Chrome (CTRL+SHIFT+I) */
					/* #earthNavigationElements */
					#earthRelativeElements,
					#legalCountry,
					#googleLogo {
						display: none !important;
					}
				`;
				document.body.appendChild(css);
			});
			await sleep(5000);
			await client.saveScreenshot(path.join(__dirname, `scr-${i}-${coords[i][0]}-${coords[i][1]}.png`));

			i += 1;
		}

		return client.end();
	} catch (err) {
		console.error(err);
	}
};
init(urls);

// Limit user drag in Google Maps API v3:
// https://stackoverflow.com/questions/3818016/google-maps-v3-limit-viewable-area-and-zoom-level
// http://jsfiddle.net/cse_tushar/9d4jy4ye/
// https://developers.google.com/maps/documentation/javascript/reference?csw=1#LatLngBounds
