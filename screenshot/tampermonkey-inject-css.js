// ==UserScript==
// @name         Inject CSS for Google Earth
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  try to take over the world!
// @author       You
// @include https://earth.google.com/*
// @include *
// @grant GM_addStyle
// @grant GM_setValue
// @grant GM_getValue
// ==/UserScript==

// https://stackoverflow.com/questions/23683439/gm-addstyle-equivalent-in-tampermonkey

//GM_addStyle('#earthRelativeElements, #legalCountry, #googleLogo { display: none !important; }');

(function() {
    'use strict';

	// Your code here...
	/*document.getElementById('earthRelativeElements').style.display = 'none';
	document.getElementById('legalCountry').style.display = 'none';
	document.getElementById('googleLogo').style.display = 'none';*/

	document.getElementById('earthRelativeElements').style.setProperty('display', 'none', 'important');
	document.getElementById('legalCountry').style.setProperty('display', 'none', 'important');
	document.getElementById('googleLogo').style.setProperty('display', 'none', 'important');

	var s1 = document.getElementById('earthRelativeElements');
	s1.style.setProperty('display', 'none', 'important');

	var s02 = document.getElementsByTagName('horizontal layout')[0];

	var s03 = document.querySelector('.horizontal');
	var s04 = document.getElementsByClassName('horizontal')[0];

	/*function addGlobalStyle(css) {
		var style = document.createElement('style');
		style.type = 'text/css';
		style.innerHTML = css.replace(/;/g, ' !important;');

		//var head = document.getElementsByTagName('head')[0];
		//if (!head) { return; }
		//head.appendChild(style);

		var html = document.getElementsByTagName('html')[0];
		html.appendChild(style);
	}
	addGlobalStyle('#earthRelativeElements, #legalCountry, #googleLogo { display: none !important; }');*/
})();
