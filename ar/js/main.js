/*global THREE, THREEx, Stats*/

'use strict';

/*
	TODO (es2015, stage-2, Minify):
	https://babeljs.io/repl/

	Qr Code decoder:
	https://zxing.org/w/decode.jspx

	FBX to COLLADA:
	https://www.autodesk.com/products/fbx/fbx-review

	https://magyarkert.com/ar/?m=kecskemet-cifrapalota&debug=true

	MODEL LIST:
	https://magyarkert.com/ar/?m=parlament&debug=true
	https://magyarkert.com/ar/?m=sandor-palota&debug=true

	https://magyarkert.com/ar/?m=space-mouse
	https://magyarkert.com/ar/?m=photoscan

	https://magyarkert.com/ar/?m=test
*/

/*
	rollup with Three.js
	https://gist.github.com/Rich-Harris/79a02519fb00837e8d5a4b24355881c0
	https://github.com/mtmckenna/three-rollup-starter
	https://github.com/Itee/threejs-full-es6
	https://duske.me/using-rollup-js-with-gulp-js/
	https://lazamar.github.io/up-and-running-with-rollup-js-in-gulp-grunt-and-native-js-api/

	Using examples without modules:
		window.THREE = require('three');
		require('three/examples/js/exporters/OBJLoader');
		const loader = new THREE.OBJLoader();

*/

/*
	QR Code Scanners:
	https://play.google.com/store/apps/details?id=tool.scanner.qrscan.barcodescan

	Marker training:
	https://archive.artoolkit.org/documentation/doku.php?id=3_Marker_Training:marker_about

	Multi markers:
	https://medium.com/arjs/area-learning-with-multi-markers-in-ar-js-1ff03a2f9fbe
	https://jeromeetienne.github.io/AR.js/three.js/examples/multi-markers/examples/player.html#%7B%22trackingBackend%22%3A%22artoolkit%22%7D
	https://www.youtube.com/watch?v=NyZ_dAlNBOI
	https://www.youtube.com/watch?v=K4yXpkoih2o
	https://www.youtube.com/watch?v=6DeouGKdfvI
	https://www.youtube.com/watch?v=KBneSfnXfZE
*/

// Avoiding `console` errors not needed anymore
// https://caniuse.com/#feat=console-basic

// workaround for chrome bug:
// http://code.google.com/p/chromium/issues/detail?id=35980#c12
if (window.innerWidth === 0) {
	window.innerWidth = parent.innerWidth;
	window.innerHeight = parent.innerHeight;
}

THREEx.ArToolkitContext.baseURL = './';

let renderer = null;
const onRenderFcts = []; // array of functions for the rendering loop
let scene = null;
let camera = null;
let controls = null;
let composer = null;
let stats = null;

//const animate = null;

let mixer = null; // https://github.com/donmccurdy/three.js/blob/625217dddf723bd30b0c9ee912663a30c36281d5/examples/webgl_loader_gltf.html
const clock = new THREE.Clock();

let depthMaterial = null;
let depthTarget = null;

// Method definitions in object literals
// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Functions/Method_definitions
const app = {
	// Update DOM on a Received Event
	init(id) {
		const self = this;

		////////////////////////////////////////////////////////////////////////
		// Init
		////////////////////////////////////////////////////////////////////////

		// Polyfill
		// https://github.com/BasqueVoIPMafia/cordova-plugin-iosrtc
		// https://github.com/revolunet/ios-webrtc
		// https://github.com/webrtc/adapter
		// https://github.com/Temasys/AdapterJS
		// https://github.com/mozdevs/mediaDevices-getUserMedia-polyfill
		// https://github.com/BasqueVoIPMafia/cordova-plugin-iosrtc
		/*if (self.queryString().debug === 'true') {
			const pretty = [];
			navigator.mediaDevices.enumerateDevices()
				.then((devices) => {
					devices.forEach((device) => {
						pretty.push(device.kind);
					});
				})
				.then(() => {
					document.getElementById('json').innerHTML = JSON.stringify({pretty: pretty}, null, 2);
					console.log(pretty);
				});
		}*/

		// FXAA
		// https://github.com/timoxley/threejs/blob/master/examples/webgl_materials_normalmap2.html
		// Anisotropic
		// https://threejs.org/examples/webgl_materials_texture_anisotropy.html
		// https://github.com/timoxley/threejs/blob/master/examples/webgl_materials_texture_anisotropy.html

		// init renderer
		renderer = new THREE.WebGLRenderer({
			antialias: false,
			alpha: true,
			logarithmicDepthBuffer: true, // logarithmic z-buffer
		});
		renderer.setClearColor(0xcccccc, 0);

		const pixelRatio = window.devicePixelRatio || 1;
		renderer.setPixelRatio(pixelRatio);
		const SCREEN_WIDTH = window.innerWidth * pixelRatio;
		const SCREEN_HEIGHT = window.innerHeight * pixelRatio;
		renderer.setSize(SCREEN_WIDTH, SCREEN_HEIGHT);
		if (self.queryString().debug === 'true') {
			const pretty = {
				'SCREEN_WIDTH': SCREEN_WIDTH,
				'SCREEN_HEIGHT': SCREEN_HEIGHT,
				'window.devicePixelRatio': window.devicePixelRatio,
			};
			document.getElementById('json').innerHTML = JSON.stringify({pretty: pretty}, null, 2);
			console.log(pretty);
		}

		/*renderer.domElement.style.position = 'absolute';
		renderer.domElement.style.top = '0px';
		renderer.domElement.style.left = '0px';*/

		const container = document.getElementById(id);
		container.appendChild(renderer.domElement);

		// array of functions for the rendering loop
		//const onRenderFcts = [];

		// init scene and camera
		scene = new THREE.Scene();

		// Darkness fix
		renderer.gammaFactor = 2.2;
		renderer.gammaOutput = true;
		console.log(`renderer.gammaFactor = ${renderer.gammaFactor}`);
		console.log(`renderer.gammaOutput = ${renderer.gammaOutput}`);

		// effectSSAO
		renderer.gammaInput = true;
		//renderer.gammaOutput = true;
		renderer.physicallyBasedShading = true;
		renderer.shadowMap.enabled = true;
		//renderer.shadowMap.cascade = true;
		renderer.shadowMap.type = THREE.PCFSoftShadowMap;
		//renderer.shadowMap.debug = true;
		//renderer.shadowMapSlopeDepthBias = true;
		//renderer.shadowMap.renderReverseSided = THREE.CullFaceBack;

		renderer.toneMappingExposure = 1;

		// Adding lights
		//const ambient = new THREE.AmbientLight(0x222222);
		//const ambient = new THREE.AmbientLight(0x666666);
		//const ambient = new THREE.AmbientLight(0x404040, 1); // soft white light
		const ambient = new THREE.AmbientLight(0xffffff, 0.75);
		scene.add(ambient);

		//var color = 0xffeedd; // warmer orange
		//const color = 0xdfebff; // cold blue
		//const color = 0xffffff;
		//const lightMainWarm = new THREE.DirectionalLight(0xfffbf7, 1.5);
		//const lightMainWarm = new THREE.DirectionalLight(0xffffff, 0.75);
		//const lightMainWarm = new THREE.DirectionalLight(0xffffff, 1);
		const lightMainWarm = new THREE.DirectionalLight(0xffeedd, 1); // warmer orange
		//lightMainWarm.position.set(0, 0, 1);
		lightMainWarm.position.set(0.5, 0, 0.866); // ~60º
		lightMainWarm.castShadow = true;
		scene.add(lightMainWarm);
		/*const lightKickerCold = new THREE.DirectionalLight(0x66a1ff, 0.5);
		lightKickerCold.position.set(0, 5, -5);
		lightKickerCold.castShadow = false;
		scene.add(lightKickerCold);*/

		/*const helper1 = new THREE.DirectionalLightHelper(light1, 5);
		scene.add(helper1);
		const helper2 = new THREE.DirectionalLightHelper(light2, 5);
		scene.add(helper2);*/

		//const cameraHelper = new THREE.CameraHelper(lightMainWarm.shadow.camera);
		//scene.add(cameraHelper);

		////////////////////////////////////////////////////////////////////////
		// Initialize a basic camera
		////////////////////////////////////////////////////////////////////////

		// Create a camera
		//const camera = new THREE.Camera();
		// https://threejs.org/docs/#api/cameras/PerspectiveCamera
		camera = new THREE.PerspectiveCamera(45, SCREEN_WIDTH / SCREEN_HEIGHT, 1, 1000);
		scene.add(camera);

		camera.aspect = SCREEN_WIDTH / SCREEN_HEIGHT;
		camera.updateProjectionMatrix();

		const audio = {
			'kecskemet-cifrapalota': 'ambient-city-church.webm',
			'parlament': 'ambient-city-bus.webm',
		};
		if (self.queryString().model in audio) {
			console.log(`${self.queryString().model} key EXISTS in audio object!`);
			// Create an AudioListener and add it to the camera
			const listener = new THREE.AudioListener();
			camera.add(listener);
			// create a global audio source
			const sound = new THREE.Audio(listener);
			const audioLoader = new THREE.AudioLoader();
			//Load a sound and set it as the Audio object's buffer
			const audioURL = `${THREEx.ArToolkitContext.baseURL}audio`;
			audioLoader.load(`${audioURL}/${audio[self.queryString().model]}`, (buffer) => {
				sound.setBuffer(buffer);
				sound.setLoop(true);
				sound.setVolume(1);
				sound.play();
			});
		} else {
			console.log(`${self.queryString().model} key NOT EXISTS in audio object!`);
		}

		// Add OrbitControls for debugging
		// https://github.com/mrdoob/three.js/blob/master/examples/misc_controls_orbit.html

		////////////////////////////////////////////////////////////////////////
		// Start of AR.js
		////////////////////////////////////////////////////////////////////////

		// Create atToolkitContext
		const arToolkitContext = new THREEx.ArToolkitContext({
			// AR backend - ['artoolkit', 'aruco', 'tango']
			cameraParametersUrl: `${THREEx.ArToolkitContext.baseURL}data/camera_para.dat`,
			// the mode of detection - ['color', 'color_and_matrix', 'mono', 'mono_and_matrix']
			detectionMode: 'mono',
			maxDetectionRate: 30,
			canvasWidth: 80 * 3,
			canvasHeight: 60 * 3,
			imageSmoothingEnabled: true,
			//debug: true,
		});

		// Start of handle arToolkitSource
		const arToolkitSource = new THREEx.ArToolkitSource({
			sourceType: 'webcam', // to read from the webcam
		});
		function onResize() {
			arToolkitSource.onResizeElement();
			arToolkitSource.copyElementSizeTo(renderer.domElement);
			if (arToolkitContext.arController !== null) {
				arToolkitSource.copyElementSizeTo(arToolkitContext.arController.canvas);
			}
		}
		arToolkitSource.init(() => {
			onResize();
		});
		// handle resize
		window.addEventListener('resize', () => {
			onResize();
		});
		// End of handle arToolkitSource

		// initialize it
		arToolkitContext.init(() => {
			// copy projection matrix to camera
			camera.projectionMatrix.copy(arToolkitContext.getProjectionMatrix());
		});

		// update artoolkit on every frame
		onRenderFcts.push(() => {
			if (arToolkitSource.ready === false) return;
			arToolkitContext.update(arToolkitSource.domElement);
		});

		// Create a ArMarkerControls
		const markerRoot = new THREE.Group();
		scene.add(markerRoot);
		const markerControls = new THREEx.ArMarkerControls(arToolkitContext, markerRoot, {
			type: 'pattern',
			patternUrl: `${THREEx.ArToolkitContext.baseURL}data/patt.hiro`,
			//patternUrl: `${THREEx.ArToolkitContext.baseURL}data/patt.kanji`,
		});

		// build a smoothedControls
		const smoothedRoot = new THREE.Group();
		scene.add(smoothedRoot);
		const smoothedControls = new THREEx.ArSmoothedControls(smoothedRoot, {
			lerpPosition: 0.4,
			lerpQuaternion: 0.3,
			lerpScale: 1,
		});
		onRenderFcts.push((delta) => {
			smoothedControls.update(markerRoot);
		});
		// smoothedControls debug
		smoothedControls.addEventListener('becameVisible', () => {
			console.log('becameVisible event notified');
		});
		smoothedControls.addEventListener('becameUnVisible', () => {
			console.log('becameUnVisible event notified');
		});

		////////////////////////////////////////////////////////////////////////
		// Add an object in the scene
		// http://github.khronos.org/glTF-WebGL-PBR/
		////////////////////////////////////////////////////////////////////////

		//const arWorldRoot = markerRoot;
		const arWorldRoot = smoothedRoot;

		// If you're testing in your own scene, make sure to include an envMap. MeshStandardMaterial tends to look better with that.

		// https://threejs.org/docs/index.html#api/textures/Texture.anisotropy
		const maxAnisotropy = renderer.capabilities.getMaxAnisotropy();
		const modelURL = `${THREEx.ArToolkitContext.baseURL}models/${self.queryString().model}`;
		//const modelAO = `${modelURL}/model-ao.jpg`;
		const loader = new THREE.GLTFLoader();
		loader.load( // https://github.com/mrdoob/three.js/blob/master/examples/webgl_materials_standard.html
			// resource URL
			`${modelURL}/model.glb`,
			// called when the resource is loaded
			(data) => {
				const gltf = data;
				const object = gltf.scene || gltf.scenes[0];
				const clips = gltf.animations || [];
				// Default material:
				// https://threejs.org/docs/#api/materials/MeshStandardMaterial
				object.updateMatrixWorld();
				object.traverse((node) => {
					if (node.isMesh === true && node.material.map !== null) {
						node.material.map.anisotropy = maxAnisotropy;
						//node.material.map.encoding = THREE.LinearEncoding;
						node.material.map.encoding = THREE.sRGBEncoding;
						//node.material.transparent = false;
						// Settings
						//node.material.metalness = 0;
						//node.material.roughness = 0;
						//node.material.wireframe = true;
						// Shading
						node.geometry.computeVertexNormals();
						////node.material.shading = THREE.SmoothShading; // <=r86
						node.material.flatShading = false; // r87+

						//node.material.aoMap = modelAO;
						//node.material.aoMapIntensity = 1;

						node.material.needsUpdate = true;
					}
					if (self.queryString().debug === 'true') {
						console.log(node);
					}
				});
				object.dithering = true;
				//object.castShadow = true;
				//object.receiveShadow = true;

				const scale = 2;
				object.scale.set(scale, scale, scale);
				//object.rotation.y = 180 * (Math.PI / 180);

				object.update = true;

				const box = new THREE.Box3().setFromObject(object);
				//const size = box.getSize().length();
				const center = box.getCenter();

				object.position.x += (object.position.x - center.x);
				object.position.y += (object.position.y - center.y);
				object.position.z += (object.position.z - center.z);

				//this.setClips(clips);

				arWorldRoot.add(object);
				//console.log(object);
			},
			// called when loading is in progresses
			(xhr) => {
				console.log(`${(xhr.loaded / xhr.total) * 100}% loaded`);
			},
			// called when loading has errors
			(error) => {
				console.log(`An error happened: ${error}`);
			},
		);

		////////////////////////////////////////////////////////////////////////
		// Post-processing
		// https://github.com/takahirox/takahirox.github.io/blob/master/three.js.mmdeditor/examples/webgl_postprocessing_taa.html
		// https://threejs.org/examples/webgl_postprocessing_taa.html
		////////////////////////////////////////////////////////////////////////

		// TAA
		/*const composer = new THREE.EffectComposer(renderer);

		camera.aspect = SCREEN_WIDTH / SCREEN_HEIGHT;
		camera.updateProjectionMatrix();
		renderer.setSize(SCREEN_WIDTH, SCREEN_HEIGHT);
		const pixelRatio = renderer.getPixelRatio();
		const newWidth = Math.floor(SCREEN_WIDTH / pixelRatio) || 1;
		const newHeight = Math.floor(SCREEN_HEIGHT / pixelRatio) || 1;
		composer.setSize(newWidth, newHeight);

		const taaRenderPass = new THREE.TAARenderPass(scene, camera);
		taaRenderPass.unbiased = false;
		taaRenderPass.sampleLevel = 3;
		composer.addPass(taaRenderPass);

		const renderPass = new THREE.RenderPass(arWorldRoot, camera);
		renderPass.enabled = false;
		composer.addPass(renderPass);

		const copyPass = new THREE.ShaderPass(THREE.CopyShader);
		copyPass.renderToScreen = true; // latest pass
		composer.addPass(copyPass);*/

		// https://github.com/mrdoob/three.js/blob/master/examples/webgl_materials_normalmap.html
		// Creating envMap
		// https://github.com/mrdoob/three.js/blob/master/examples/webgl_loader_gltf.html
		// https://github.com/donmccurdy/three-gltf-viewer/blob/master/src/viewer.js

		/*const renderTargetParametersRGB = { minFilter: THREE.LinearFilter, magFilter: THREE.LinearFilter, format: THREE.RGBFormat };
		const renderTargetParametersRGBA = { minFilter: THREE.LinearFilter, magFilter: THREE.LinearFilter, format: THREE.RGBAFormat };
		const depthTarget = new THREE.WebGLRenderTarget(SCREEN_WIDTH, SCREEN_HEIGHT, renderTargetParametersRGBA);
		const colorTarget = new THREE.WebGLRenderTarget(SCREEN_WIDTH, SCREEN_HEIGHT, renderTargetParametersRGBA);
		// Init composer
		//renderer.autoClear = false;
		composer = new THREE.EffectComposer(renderer);
		//composer = new THREE.EffectComposer(renderer, colorTarget);
		composer.setSize(SCREEN_WIDTH, SCREEN_HEIGHT);
		// Render model
		//composer.addPass(new THREE.RenderPass(scene, camera)); // render model
		const renderPass = new THREE.RenderPass(scene, camera);
		composer.addPass(renderPass);*/

		// depth
		//const depthShader = THREE.ShaderLib['depthRGBA'];
		/*const depthShader = THREE.ShaderLib['distanceRGBA'];
		console.log(depthShader);
		const depthUniforms = THREE.UniformsUtils.clone(depthShader.uniforms);
		depthMaterial = new THREE.ShaderMaterial({
			fragmentShader: depthShader.fragmentShader,
			vertexShader: depthShader.vertexShader,
			uniforms: depthUniforms,
		});
		// postprocessing
		composer = new THREE.EffectComposer(renderer);
		const renderPass = new THREE.RenderPass(scene, camera);
		composer.addPass(renderPass);
		depthTarget = new THREE.WebGLRenderTarget(512, 512, {
			minFilter: THREE.LinearFilter,
			magFilter: THREE.LinearFilter,
			format: THREE.RGBAFormat,
		});*/

		renderer.autoClear = false;
		composer = new THREE.EffectComposer(renderer);
		//composer = new THREE.EffectComposer(renderer, colorTarget);
		composer.setSize(SCREEN_WIDTH, SCREEN_HEIGHT); // multiply with devicePixelRatio
		// Render model
		const renderPass = new THREE.RenderPass(scene, camera);
		composer.addPass(renderPass);

		// FXAA
		const shaderPass = new THREE.ShaderPass(THREE.FXAAShader);
		shaderPass.uniforms['resolution'].value.set(1 / SCREEN_WIDTH, 1 / SCREEN_HEIGHT);
		composer.addPass(shaderPass);
		// Bleach (Contrast)
		/*const effectBleach = new THREE.ShaderPass(THREE.BleachBypassShader);
		effectBleach.uniforms['opacity'].value = 0.1;
		composer.addPass(effectBleach);*/
		// Color correction
		const effectColor = new THREE.ShaderPass(THREE.ColorCorrectionShader);
		effectColor.uniforms['addRGB'].value.set(0, 0, 0);
		effectColor.uniforms['mulRGB'].value.set(1.2, 1.2, 1.2);
		effectColor.uniforms['powRGB'].value.set(1.4, 1.4, 1.4);
		composer.addPass(effectColor);
		console.log(effectColor);
		// SAO
		// https://github.com/mrdoob/three.js/blob/dev/examples/webgl_postprocessing_sao.html
		/*const saoPass = new THREE.SAOPass(scene, camera, false, true);
		console.log(saoPass);
		saoPass.params.saoBias = 0.5;
		saoPass.params.saoIntensity = 0.18;
		saoPass.params.saoScale = 70;
		saoPass.params.saoKernelRadius = 100;
		saoPass.params.saoMinResolution = 0;
		saoPass.params.saoBlur = false;
		saoPass.params.saoBlurRadius = 8;
		saoPass.params.saoBlurStdDev = 4;
		saoPass.params.saoBlurDepthCutoff = 0.01;
		//saoPass.renderToScreen = true;
		composer.addPass(saoPass);*/
		// Render to screen
		const copyPass = new THREE.ShaderPass(THREE.CopyShader);
		copyPass.renderToScreen = true; // Place in latest ShaderPass
		composer.addPass(copyPass);

		////////////////////////////////////////////////////////////////////////
		// Render the whole thing on the page
		////////////////////////////////////////////////////////////////////////

		if (self.queryString().debug === 'true') {
			console.log(`Received Event: ${id}`);
			console.log(composer);
		}
	},

	animate() {
		self = this;

		// run the rendering loop
		/*let lastTimeMsec = null;
		requestAnimationFrame(function animate(nowMsec) {
			// keep looping
			requestAnimationFrame(animate);
			// measure time
			lastTimeMsec = lastTimeMsec || nowMsec - 1000 / 60;
			const deltaMsec = Math.min(200, nowMsec - lastTimeMsec);
			lastTimeMsec = nowMsec;
			// call each update function
			onRenderFcts.forEach((onRenderFct) => {
				onRenderFct(deltaMsec / 1000, nowMsec / 1000);
			});
		});*/

		// https://threejs.org/docs/#api/core/Clock
		requestAnimationFrame(function animate() {
			requestAnimationFrame(animate);
			onRenderFcts.forEach((onRenderFct) => {
				onRenderFct(clock.getDelta() / 1000, clock.getElapsedTime() / 1000);
			});
		});

		if (mixer) mixer.update(clock.getDelta());

		self.render();
	},

	render() {
		const self = this;

		// render the scene
		onRenderFcts.push(() => {
			//renderer.render(scene, camera);
			composer.render();
		});

		if (self.queryString().debug === 'true') {
			stats = new Stats();
			document.body.appendChild(stats.dom);
			onRenderFcts.push(() => {
				stats.update();
			});

			/*
				3cm at 300dpi = 354px
				0.5cm at 300dpi = 59px
				2.5cm at 300dpi = 295px
			*/
			self.qrCodeDisplay(self.queryString().model, 295, 30);
		}
	},

	// GET url variables
	// https://css-tricks.com/snippets/javascript/get-url-variables/
	/*getQueryString(name, url) {
		if (!url) url = window.location.href;
		name = name.replace(/[\[\]]/g, '\\$&');
		var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)');
		var results = regex.exec(url);
		if (!results) return null;
		if (!results[2]) return '';
		return decodeURIComponent(results[2].replace(/\+/g, ' '));
	},*/
	/*getQueryString(variable) {
		const query = window.location.search.substring(1);
		const vars = query.split('&');
		for (let i = 0; i < vars.length; i += 1) {
			const pair = vars[i].split('=');
			if (pair[0] === variable) {
				return decodeURIComponent(pair[1]);
			}
		}
		return false;
	},*/
	/*getQueryString(variable) { // Functional approach
		return window.location.search.substring(1).split('&')
			.map(p => p.split('='))
			.filter(p => p[0] === variable)
			.map(p => decodeURIComponent(p[1]))
			.pop();
	},*/
	getQueryString(name) {
		name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
		const regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
		const results = regex.exec(window.location.search);
		return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
	},
	queryString() {
		// https://alistapart.com/article/getoutbindingsituations
		// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/prototype
		// https://gist.github.com/jashmenn/b306add36d3e6f0f6483
		const self = this; // closure
		const model = self.getQueryString('m');
		const debug = self.getQueryString('debug');
		const data = {
			model,
			debug,
		};
		return data;
	},

	/*
		// Dynamic script loader with integrated callback hell

		// Usage:
		scriptLoader('a1.js', function () {
			scriptLoader('a2.js', function () {
				console.log('Hello');
			});
		});
	*/
	scriptLoader(filename, callback) {
		const head = document.getElementsByTagName('head')[0];
		const script = document.createElement('script');
		script.type = 'text/javascript';
		script.src = filename;
		script.defer = true;
		if (typeof callback === 'function') {
			script.onload = callback;
		}
		return head.appendChild(script);
	},

	qrCodeDisplay(url, size, border) {
		const self = this;
		return self.scriptLoader('../../ar-qr-common/js/vendor/jquery.min.js', () => {
			self.scriptLoader('../../ar-qr-common/js/vendor/jquery.qrcode.src/qrcode.js', () => {
				self.scriptLoader('../../ar-qr-common/js/vendor/jquery.qrcode.src/jquery.qrcode.js', () => {
					const script = document.createElement('script');
					script.type = 'text/javascript';
					script.text = `
						var qrData = {
							size: ${size},
							url: "https://magyarkert.com/ar/?m=${url}",
							border: ${border},
						};
						jQuery('#qrcodeCanvas').qrcode({
							render: "canvas",
							width: qrData.size,
							height: qrData.size,
							border: qrData.border,
							text: qrData.url,
						});
					`;
					script.defer = true;
					const head = document.getElementsByTagName('head')[0];
					head.appendChild(script);
				});
			});
		});
	},

	// deviceready Event Handler
	onDeviceReady() {
		const self = this;
		self.init('deviceready');
		//self.render();
		self.animate();
	},

	// Application Constructor
	initialize() {
		const self = this;
		document.addEventListener('DOMContentLoaded', self.onDeviceReady(), false);
	},
};

app.initialize();
