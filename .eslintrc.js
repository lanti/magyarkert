// http://eslint.org/docs/user-guide/configuring
// http://eslint.org/docs/rules/
// http://eslint.org/docs/user-guide/configuring#configuring-rules
// http://kangax.github.io/compat-table/es5/
// https://kangax.github.io/compat-table/es6/
// http://caniuse.com/#feat=es5

const path = require('path');

module.exports = {
  root: true,

  // https://www.npmjs.com/package/babel-eslint
  // https://www.npmjs.com/package/eslint-plugin-babel
  parser: 'babel-eslint',
  plugins: [
    'babel',
  ],

  //'extends': 'eslint:recommended',
  extends: 'airbnb-base',

  globals: {
    jQuery: true,
    $: true
  },

  env: {
    es6: true,
    browser: true,
    node: true,
    jquery: true
  },

  parserOptions: {
    ecmaVersion: 6,
    sourceType: 'script', // 'script' or 'module'
    ecmaFeatures: {
      jsx: false,
      generators: true,
      experimentalObjectRestSpread: true
    }
  },

  settings: {
    'import/resolver': {
      webpack: {
        //config: 'webpack.common.js',
        //config: 'webpack.dev.js',
        config: path.join(__dirname, 'webpack.common.js'),
        //'config-index': 1,
        /*config: {
          resolve: {
            extensions: ['.js', '.json'],
          },
        },*/
      },
    },
  },

  rules: {
    'spaced-comment': 'off',
    //'import/no-extraneous-dependencies': ['error', { 'devDependencies': true }],
    //'no-param-reassign': ['error', { 'props': false }],
    'no-tabs': 'off',
    "indent": [2, "tab", { "SwitchCase": 1, "VariableDeclarator": 1 }],
  },
}
