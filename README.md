# Magyarkert

Magyarkert AR and QR Code projects

Inspiration:

https://github.com/yagiz/Andromeda

three.ar.js marker example:

https://github.com/google-ar/three.ar.js/pull/83

# aframe + Argon Browser = GPS based AR:

```sh
https://aframe.io/blog/awoa-35-39/
https://docs.argonjs.io/
https://aframe.argonjs.io/
https://www.argonjs.io/
https://app.argonjs.io/
https://www.vuforia.com/
https://unity3d.com/partners/vuforia
https://www.vuforia.com/press-releases/ptc-announces-major-new-release-to-vuforia-augmented-reality-platform.html
https://jesstelford.github.io/aframe-map/poi/
https://samples.argonjs.io/streetview/index.html
https://samples.argonjs.io/geopose/index.html
```

# Three.js webassembly loader

```sh
https://github.com/google/draco/issues/94
https://github.com/google/draco/issues/93
https://github.com/google/draco/blob/master/javascript/example/webgl_loader_draco.html
```

# TODO

* Enable caching on Cloudflare for automatic minifcation: https://support.cloudflare.com/hc/en-us/articles/200172256-How-do-I-cache-static-HTML-

## Agisoft PhotoScan Pro to GLTF export

1. Agisoft PhotoScan Pro
	* Right Click on Workspace -> Duplicate -> Rename
	* Tools -> Mesh -> Decimate Mesh (2000 target)
	* Workspace -> Add marker (4 corners)
	* Reference -> Enter 4 coordinates -> Hit refresh
	* File -> Export -> Export Model (DAE)
2. Sketchup
	* Soften Edges (Smooth normals, Soften coplanar, 90 degrees)
	* [Bomb All](https://extensions.sketchup.com/en/content/bomb)
	* Resize model (2 meters for long models, 1 meters for tall)
	* [Resize Textures (selected materials on the whole model, percentage 50%, max 512px, min 512px, quality 60%)](https://extensions.sketchup.com/ru/content/texture-resizer)
	* [GLB Export (without Paint 3D)](https://extensions.sketchup.com/en/content/gltf-exporter)


## gltf to glb converter

https://github.com/sbtron/makeglb

```sh
$ npm install git+https://git@github.com/AnalyticalGraphicsInc/gltf-pipeline.git#2.0
```

## Opus WEBM conversion

```cmd
@echo off

:: https://trac.ffmpeg.org/wiki/Encode/VP9
:: http://ffmpeg.org/ffmpeg-codecs.html#libopus-1
:: https://superuser.com/questions/516806/how-to-encode-audio-with-opus-codec

ffmpeg -i ab-f-031616-38455895_city-park-europe-ambience-01.mp3 ^
    -c:a libopus ^
    -b:a 16K ^
    -vbr on ^
    -compression_level 10 ^
    output.webm

PAUSE
```

## Recap to GLTF export

1. Recap Photo OBJ export
2. In SketchUP (prepare the model):
	1. Resize to 1-2 meters
	2. Soften edges (180°, Smooth Normals, Soften Coplanar)
	3. [Bomb All](https://extensions.sketchup.com/en/content/bomb)
3. In SketchUP (export):
	1. [TT_LIB2](https://extensions.sketchup.com/en/content/tt_lib%C2%B2)
	2. [QuadFace Tools](https://extensions.sketchup.com/en/content/quadface-tools)
	3. [glTF Export](https://extensions.sketchup.com/en/content/gltf-exporter)
	4. Import -> OBJ Files (QuadFace Tools)
	5. Sketchup Extensions window -> Bomb All
	6. Export GLB without Paint3D

## Speech API

Google Cloud Platform - Speech API key:

```
# Usage: key=API_KEY
AIzaSyDxtV_2pFngsHYooG5ZM4CHzeMcwaBBvpw
```

Restriction:

```
*.example.com/*

http://mesziair.hu/*
https://mesziair.hu/*
http://magyarkert.com/*
https://magyarkert.com/*
http://www.magyarkert.com/*
https://www.magyarkert.com/*
```

User agents:

```
Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:56.0) Gecko/20100101 Firefox/56.0
Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36
```

## Webdriver.io Selenium screenshot application

Guide: http://webdriver.io/guide.html

Prerequisites:

1. Download Selenium server: http://docs.seleniumhq.org/download/
2. Download Gecko driver: https://github.com/mozilla/geckodriver/releases
3. Download Firefox driver: https://sites.google.com/a/chromium.org/chromedriver/downloads

Usage:

```
$ java -jar -Dwebdriver.gecko.driver=./vendor/geckodriver.exe ./vendor/selenium-server-standalone-3.6.0.jar
# OR
$ java -jar -Dwebdriver.chrome.driver=./vendor/chromedriver.exe ./vendor/selenium-server-standalone-3.6.0.jar
# Then
$ set MOZ_HEADLESS=1 & npm start
```

Firefox native screenshots:

1. Download nightly: https://www.mozilla.org/hu/firefox/nightly/all/

2. Run this command:

```sh
# https://wiki.mozilla.org/Nightly#How_do_I_install_Firefox_Nightly_alongside_Firefox_Release.3F_2
$ "C:\Program Files\Mozilla Firefox Nightly\firefox.exe" -P --no-remote
```

3. 

```sh
$ "C:\Program Files\Mozilla Firefox Nightly\firefox.exe" -headless -screenshot "D:\www\magyarkert\screenshot\test.png" https://mesziair.hu/ --window-size=1920,1080
$ "C:\Program Files\Mozilla Firefox Nightly\firefox.exe" -headless -screenshot "D:\www\magyarkert\screenshot\test.png" "https://www.google.com/maps/place/46%C2%B054'30.8%22N+19%C2%B041'42.8%22E/@46.9085647,19.6948024,132m/data=!3m1!1e3!4m5!3m4!1s0x0:0x0!8m2!3d46.9085436!4d19.6952169" --window-size=1920,1080
```
