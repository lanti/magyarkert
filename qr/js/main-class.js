'use strict';

/*
	URL LIST:
	http://magyarkert.com/qr/?c=jakabszallas&debug=true

	TODO:
	- SOMEHOW CACHE THE AUDIO LOOP!

	TODO (es2015, stage-2, Minify):
	https://babeljs.io/repl/

	https://developer.chrome.com/apps/tts
	https://developer.chrome.com/extensions/ttsEngine
	https://blog.chromium.org/2011/10/new-text-to-speech-api-for-chrome.html
	https://developer.chrome.com/extensions/samples#search:tts
*/

// Avoid `console` errors in browsers that lack a console.
(function () {
	var method;
	var noop = function () {};
	var methods = [
		'assert', 'clear', 'count', 'debug', 'dir', 'dirxml', 'error',
		'exception', 'group', 'groupCollapsed', 'groupEnd', 'info', 'log',
		'markTimeline', 'profile', 'profileEnd', 'table', 'time', 'timeEnd',
		'timeline', 'timelineEnd', 'timeStamp', 'trace', 'warn',
	];
	var length = methods.length;
	var console = (window.console = window.console || {});

	while (length--) {
		method = methods[length];

		// Only stub undefined methods.
		if (!console[method]) {
			console[method] = noop;
		}
	}
}());

// Hack to return voice list on the second call
// This way `window.speechSynthesis.getVoices();` will work
(function () {
	return window.speechSynthesis;
})();

// Autoplay sound on mobile
// https://www.ibm.com/developerworks/library/wa-ioshtml5/
// https://developers.google.com/web/updates/2017/09/autoplay-policy-changes
// You need an old-fashioned physical touchstart or click event,
// triggered by the user's finger (or Cardboard button) and not by a fuse or gaze cursor.
// or
// https://stackoverflow.com/questions/25023211/html5-audio-why-not-working-on-ios-no-autoplay-involved
// https://stackoverflow.com/questions/34837930/audio-tag-autoplay-not-working-in-mobile
// https://developer.mozilla.org/en-US/docs/Web/API/BaseAudioContext
// https://developer.apple.com/library/content/documentation/AudioVideo/Conceptual/Using_HTML5_Audio_Video/Device-SpecificConsiderations/Device-SpecificConsiderations.html

// Extend Date built-in
// https://github.com/GoogleChrome/samples/blob/gh-pages/classes-es6/demo.js
class MyDate extends Date {
	// JS will provide a default constructor that will call super by default.
	// So you don't need constructor in this case.
	/*constructor() {
		super();
	}*/

	getFormattedDate() {
		const months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
		return `${this.getDate()}-${months[this.getMonth()]}-${this.getFullYear()}`;
	}
}

const aDate = new MyDate();
console.log(aDate.getTime());
console.log(aDate.getFormattedDate());

class App {
	constructor() {
		this.name = 'App';
		this.id = 'deviceready2';
	}

	initialize() {
		document.addEventListener('DOMContentLoaded', this.onDeviceReady.bind(this), false);
	}

	onDeviceReady() {
		const parentElement = document.getElementById(this.id);
		parentElement.innerHTML += `
			<h1>Received Event: ${this.id}</h1>
		`;

		console.log(`Received Event: ${this.id}`);
	}
}

const app2 = new App();
app2.initialize();

const app = {
	// Application Constructor
	initialize: function () {
		var _this = this;
		document.addEventListener('DOMContentLoaded', _this.onDeviceReady.bind(_this), false);
	},

	// deviceready Event Handler
	onDeviceReady: function () {
		var _this = this;
		if (window.fetch) {
			return _this.receivedEvent('deviceready');
		} else {
			_this.scriptLoader('//rawcdn.githack.com/taylorhakes/promise-polyfill/e20aa9031b0352d695fc0428cb8bc0145ffc5519/promise.min.js', function () {
				_this.scriptLoader('//rawcdn.githack.com/github/fetch/v2.0.3/fetch.js', function () {
					//console.log("window.fetch === " + window.fetch); // undefined
					return _this.receivedEvent('deviceready');
				});
			});
		}
	},

	// Business logic starts here
	getParameterByName: function (name, url) {
		if (!url) url = window.location.href;
		name = name.replace(/[\[\]]/g, '\\$&');
		var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)');
		var results = regex.exec(url);
		if (!results) return null;
		if (!results[2]) return '';
		return decodeURIComponent(results[2].replace(/\+/g, ' '));
	},
	queryString: function () {
		var _this = this;
		var city = _this.getParameterByName('c');
		var debug = _this.getParameterByName('debug');

		var data = {
			city: city,
			debug: debug,
		};
		return data;
	},
	data: function () {
		var _this = this;
		var streams = 'data/' + _this.queryString().city + '/data.json';
		return streams;
	},

	/*
		// Dynamic script loader with integrated callback hell

		// Usage:
		scriptLoader('a1.js', function () {
			scriptLoader('a2.js', function () {
				console.log('Hello');
			});
		});
	*/
	scriptLoader: function (filename, callback) {
		var head = document.getElementsByTagName('head')[0];
		var script = document.createElement('script');
		script.type = 'text/javascript';
		script.src = filename;
		script.defer = true;
		if (typeof callback === 'function') {
			script.onload = callback;
		}
		return head.appendChild(script);
	},

	apiDisplay: function (name, description, pictures) {
		return `
			<article class="city">
				<p>${name}</p>
				<p>${description}</p>
				<p>${pictures.map(item => `<img src="${item}" alt="" />`).join('')}</p>
			</article>
		`;
	},

	qrCodeDisplay: function (url, size, border) {
		var _this = this;

		return _this.scriptLoader('js/vendor/jquery.min.js', function () {
			_this.scriptLoader('js/vendor/jquery.qrcode.src/qrcode.js', function () {
				_this.scriptLoader('js/vendor/jquery.qrcode.src/jquery.qrcode.js', function () {
					var script = document.createElement('script');
					script.type = 'text/javascript';
					script.text = "" +
						"var qrData = {" +
							"size: " + size + "," +
							"url: \"https://magyarkert.com/qr/?c=" + url + "\"," +
							"border: " + border + "," +
						"};" +
						"jQuery('#qrcodeCanvas').qrcode({" +
							"render: \"canvas\"," +
							"width: qrData.size," +
							"height: qrData.size," +
							"border: qrData.border," +
							"text: qrData.url," +
						"});" +
					"";
					script.defer = true;
					var head = document.getElementsByTagName('head')[0];
					head.appendChild(script);
				});
			});
		});
	},

	// https://www.google.com/speech-api/v1/synthesize?ie=UTF-8&text=Szia%20vil%C3%A1g!&lang=hu&pitch=0.5&speed=0.5&vol=1
	// https://www.google.com/speech-api/v1/synthesize?ie=UTF-8&text=Szia%20vil%C3%A1g!&lang=hu&pitch=0.5&speed=0.5&vol=1&name=
	// https://www.google.co.in/chrome/browser/privacy/whitepaper.html#speech

	audioPlayer: function (file) {
		var _this = this;
		var voice = document.getElementById("voice");
		var audio = document.createElement('audio');
		audio.id = 'audio-player';
		audio.controls = 'controls';
		audio.preload = 'auto';
		audio.src = file;
		audio.type = 'audio/mpeg';
		voice.appendChild(audio);
		return audio;
	},

	// 2017-10-19 20:58
	audioCache: function (text, language) {
		// https://developer.mozilla.org/en-US/docs/Web/API/HTMLAudioElement
		// https://stackoverflow.com/questions/46117568/play-sounds-from-array-every-0-5-sec
		// https://jsfiddle.net/Darker/d23q8ahj/
		var _this = this;

		var strings = text.split(/(?:\.)+\s|(?::)+\s/g);
		console.log(strings);

		var play = document.getElementById('play');
		var stop = document.getElementById('stop');

		var mySound = [];
		var currentAudio = null;

		// Preloading
		for (var i = 0; i < strings.length; i += 1) {
			mySound.push(new Audio('https://www.google.com/speech-api/v1/synthesize?ie=UTF-8&text=' + strings[i] + '&lang=' + language + '&pitch=0.5&speed=0.5&vol=1'));
			mySound[i].preload = true;
		}
		console.log(mySound);

		// Generate audio links for debug mode
		if (_this.queryString().debug === "true") {
			var elem = document.getElementById('audioLinks');

			for (var i = 0; i < mySound.length; i += 1) {
				elem.innerHTML += `
					<a href="https://www.google.com/speech-api/v1/synthesize?ie=UTF-8&text=${strings[i]}&lang=${language}&pitch=0.5&speed=0.5&vol=1">audio-${i}</a>
				`;
			}
		}

		currentAudio = mySound[0];
		//currentAudio = new Audio('https://www.google.com/speech-api/v1/synthesize?ie=UTF-8&text=' + text + '&lang=' + language + '&pitch=0.5&speed=0.5&vol=1');

		var index = 1;

		// TODO: detect if all files are fully loaded
		// https://developer.mozilla.org/en-US/docs/Web/API/HTMLMediaElement/readyState
		// https://developer.mozilla.org/en-US/docs/Web/API/EventTarget/addEventListener
		// https://developer.mozilla.org/en-US/docs/Web/API/HTMLAudioElement
		// https://developer.mozilla.org/en-US/docs/Web/Events/loadeddata
		//mySound[0].addEventListener('loadeddata', function () {
		currentAudio.addEventListener('loadeddata', function () {
			if (this.readyState === 4) {
				this.play();
			}
		});

		for (var i = 0; i < mySound.length; i += 1) {
			mySound[i].onended = function () {
				if (index < mySound.length) {
					//mySound[index].play();
					currentAudio = mySound[index];
					currentAudio.play();
					index++;
				} else {
					// Reset to start values after completion
					currentAudio = mySound[0];
					index = 1;
				}
			};
		}

		play.onclick = function () {
			currentAudio.play();
		}
		stop.onclick = function () {
			currentAudio.pause();
		}
	},

	audioLocal: function (folder) {
		var _this = this;

		var play = document.getElementById('play');
		var stop = document.getElementById('stop');

		var url = 'data/' + folder + '/audio.mp3';

		var audio = new Audio(url);
		audio.preload = true;
		//console.log(audio);

		/*audio.onloadeddata = function () {
			if (audio.readyState === 4) {
				audio.play();
			}
		};*/
		/*audio.addEventListener('loadeddata', function () {
			if (audio.readyState === 4) {
				audio.play();
			}
		});*/
		audio.autoplay = true;

		play.onclick = function () {
			audio.play();
		}
		stop.onclick = function () {
			audio.pause();
		}
	},

	audioGoogle: function (text, folder, language) {
		var _this = this;

		var strings = text.split(/(?:\.)+\s|(?::)+\s/g);
		console.log(strings);

		// Generate audio links for debug mode
		var elem = document.getElementById('audioLinks');

		for (var i = 0; i < strings.length; i += 1) {
			elem.innerHTML += `
				<a href="https://www.google.com/speech-api/v1/synthesize?ie=UTF-8&text=${strings[i]}&lang=${language}&pitch=0.5&speed=0.5&vol=1" download="${folder}-${i}.mp3">${folder}-${i}</a>
			`;
		}
	},

	webSpeechAPIMDN: function (text, language) {
		// https://github.com/mdn/web-speech-api/blob/master/speak-easy-synthesis/script.js
		// https://developers.google.com/web/updates/2014/01/Web-apps-that-talk-Introduction-to-the-Speech-Synthesis-API
		var _this = this;

		var play = document.getElementById('play3');
		var stop = document.getElementById('stop3');

		var synth = window.speechSynthesis;
		var voices = window.speechSynthesis.getVoices();
		console.log(voices);
		//console.log(`voices: ${voices[0].default}`);

		// Return the default voice id from the list
		var keys = Object.keys(voices);
		var filtered = keys.filter(function (key) {
			return voices[key].default;
		});
		console.log(filtered);

		// Return default object if undefined or the object itself if it's true
		var synthVoice = function (obj, key) {
			if (obj[key] === undefined) {
				return obj[filtered];
			} else {
				return obj[key];
			}
		}

		var msg = new SpeechSynthesisUtterance();
		//msg.voice = voices[10]; // Note: some voices don't support altering params
		msg.voice = synthVoice(voices, 0);
		msg.voiceURI = 'native';
		msg.volume = 1; // 0 to 1
		msg.rate = 0.9; // 0.1 to 10
		msg.pitch = 1; //0 to 2
		msg.text = text;
		msg.lang = language;

		play.onclick = function () {
			synth.speak(msg);
		}
		stop.onclick = function () {
			synth.cancel();
		}
	},

	voice: function (text) {
		var _this = this;
		//var voice = document.getElementById("voice");

		var strings = text.split(/(?:\.)+\s|(?::)+\s/g);
		//console.log(strings);

		/*var audio = document.createElement('audio');
		audio.id = 'audio-player';
		audio.controls = 'controls';
		audio.preload = 'auto';
		audio.src = 'https://www.google.com/speech-api/v1/synthesize?ie=UTF-8&text=' + strings[0] + '&lang=hu&pitch=0.5&speed=0.5&vol=1';
		audio.type = 'audio/mp3';
		voice.appendChild(audio);

		var index = 1;
		audio.onended = function () {
			if (index < strings.length) {
				audio.src = 'https://www.google.com/speech-api/v1/synthesize?ie=UTF-8&text=' + strings[index] + '&lang=hu&pitch=0.5&speed=0.5&vol=1';
				audio.play();
				index++;
			}
		};*/
		//_this.audioPlayer('https://www.google.com/speech-api/v1/synthesize?ie=UTF-8&text=' + strings[0] + '&lang=hu&pitch=0.5&speed=0.5&vol=1');
		/*var index = 1;
		_this.audioPlayer('https://www.google.com/speech-api/v1/synthesize?ie=UTF-8&text=' + strings[0] + '&lang=hu&pitch=0.5&speed=0.5&vol=1').onended = function () {
			if (index < strings.length) {
				this.src = 'https://www.google.com/speech-api/v1/synthesize?ie=UTF-8&text=' + strings[index] + '&lang=hu&pitch=0.5&speed=0.5&vol=1';
				this.play();
				index++;
			}
		};*/

		// TODO:
		// 1. Create an array of google urls
		// 2. fetch them through
		// 3. Store them in variable
		// 4. Init audio

		var urls = [];
		for (var i = 0; i < strings.length; i += 1) {
			urls.push('https://www.google.com/speech-api/v1/synthesize?ie=UTF-8&text=' + strings[i] + '&lang=hu&pitch=0.5&speed=0.5&vol=1');
		}
		console.log(urls);

		//var myHeaders = new Headers();
		//myHeaders.append('Content-Type', 'audio/mpeg');
		var myInit = {
			//method: 'GET',
			//headers: myHeaders,
			mode: 'no-cors',
			//cache: 'default',
			//credentials: 'omit',
		};
		// https://gist.github.com/revolunet/e620e2c532b7144c62768a36b8b96da2
		// https://developer.mozilla.org/en-US/docs/Web/API/Body/blob
		// TODO: AJAX
		var promises = urls.map((url) => {
			var myRequest = new Request(url);
			fetch(myRequest, myInit)
				.then(function (response) {
					//if (response.status == 200) return response.blob();
					//else throw new Error('Something went wrong on api server!');
					return response.blob();
				})
				.catch(function (error) {
					console.error('fetch(catch): ' + error);
				});
		});
		Promise.all(promises)
			.then((response) => {
				var objectURL = URL.createObjectURL(response);
				console.log(objectURL);
			})
			.catch(function (error) {
				console.error('Promise.all(catch): ' + error);
			});
	},

	// Update DOM on a Received Event
	receivedEvent: function (id) {
		var _this = this;
		const app = document.getElementById(id);
		//var lang = 'en-US';
		var lang = 'hu-HU';

		return fetch(_this.data())
			.then(function (response) {
				if(response.ok) return response.json();
				else throw new Error('Looks like there was a problem. Status Code: ' + response.status);
			})
			.then(function (json) {
				app.innerHTML += _this.apiDisplay(
					json.name,
					json.description,
					json.pictures,
				);

				//_this.audioCache(json.audio[lang], lang);
				//_this.webSpeechAPIMDN(json.audio[lang]);
				_this.audioLocal(json.url);

				//console.log(`${JSON.stringify(json, null, 4)}`);
				if (_this.queryString().debug === 'true') {
					/*
						3cm at 300dpi = 354px
						0.5cm at 300dpi = 59px
						2.5cm at 300dpi = 295px
					*/
					_this.qrCodeDisplay(json.url, 295, 30);
					console.log('Received Event: ' + id);

					// Generate audio download links
					_this.audioGoogle(json.audio[lang], json.url, lang);
				}
			})
			.catch(function (error) {
				app.innerHTML = '<h1>404</h1><h3>Az oldal nem létezik</3>';
				console.error('fetch(catch): ', error);
			});
	}
};

app.initialize();
